# Javni portal za sodelovanje skupnosti pri trajnostni rabi energije

Ta repozitorij vsebuje kodo za javni portal, ki sem ga razvil v okviru magistrske naloge.
Javni portal je implementiran v obliki Angular spletne aplikacije.

## Vzpostavitev okolja

Ta projekt je bil postavljen z [Angular CLI](https://github.com/angular/angular-cli) verzije 13.2.4.

Če na računalniku ni še nameščen Angular CLI, ga namestimo z ukazom `npm install -g @angular/cli`.

Za namestitev knjižnjic, ki so uporabljene v projektu poženemo ukaz `npm install`.

### Firebase

Javni portal omogoča interaktivnost preko nadzorne plošče, ki je namenjena prikazu na mobilnem telefonu ali tabličnem računalniku.
Za komunikacijo med javnim prikazom in nadzorno ploščo smo uporabili storitev Firebase Realtime Database.

Za uspešen zagon spletne aplikacije javnega portala je zato potrebno ustvariti projekt v konzoli [platforme Firebase](https://console.firebase.google.com/).

* Dodamo nov projekt, izberemo lahko kakršnokoli ime projekta
* Omogočimo storitev Realtime Database in nastavimo pravila dostopa na vrednost `true`, s čimer omogočimo komunikacijo brez prijave ali dodatnih omejitev.
* Na domači strani projekta (Project Overview) dodamo aplikacijo, izberemo opcijo "Web"
* Ko je aplikacija dodana v Firebase pridobimo podatke o konfiguraciji, ki jih kopiramo na označeno mesto v datoteki `src/environments/environment.ts`.

## Strežnik za razvoj in lokalno testiranje

Strežnik za razvijanje se zažene z ukazom `ng serve`. Aplikacija je dostopna preko brskalnika na `http://localhost:4200/prkz`. Spletna aplikacija se avtomatsko osveži, ko prido do kakršnekoli spremembe v kodi.

Aplikacija ima še nekaj podstrani:

* Nadzorna plošča: `http://localhost:4200/dashboard-home`
* Nastavljanje trenutno prikazanega časa: `http://localhost:4200/ambient-time`
* Osnovna stran z izobraževalno vsebino in vprašalnikom: `http://localhost:4200`

## Firebase Hosting
Za nalaganje spletne aplikacije na storitev Firebase Hosting je potrebno stoterv omogočiti v Firebase konzoli. Za nalaganje spletne aplikacije na strežnik je potrebno najprej pognati ukaz `ng build --aot` in nato še `ng deploy` ali `firebase deploy`.
