import {Component, QueryList, ViewChildren} from "@angular/core";
import {Chart, ChartConfiguration, ChartData, ChartType} from "chart.js";
import {Data} from "../models/data";
import {HttpClient} from "@angular/common/http";
import {BaseChartDirective} from "ng2-charts";
import {AngularFireDatabase} from "@angular/fire/compat/database";
import {House} from "../House";
import {Router} from "@angular/router";

@Component({
  selector: 'tutorial-dashboard.component',
  templateUrl: './tutorial-dashboard.component.html',
  styleUrls: ['./tutorial-dashboard.component.scss'],
})
export class TutorialDashboardComponent {
  @ViewChildren(BaseChartDirective) charts: QueryList<BaseChartDirective> | undefined;

  public data: Data;

  houseLabels: string[] = ["Waiting", "Waiting"];
  weatherLabel: string = "Waiting";
  useBattery: boolean = true;

  selectedTab: number = 0;

  constructor(
    private http: HttpClient,
    private db: AngularFireDatabase,
    private router: Router
  ) {
    this.data = new Data(this.http);
    this.data.getHouseData('assets/archetypes_exp.json');
    this.data.getSolarData('assets/pv_gen.json');

    Chart.defaults.color = "#d1d1d1";

    setTimeout(() => {
      // Update chart
      this.loadProfileChartData.labels = this.data.timestamps;
      this.loadProfileChartData.datasets = [];

      this.loadProfileChartData.datasets.push(
        {
          data: this.data.houses[3].loadProfile,
          label: "Leva hiša",
          cubicInterpolationMode: 'monotone',
          tension: 0.9
          // borderRadius: 2
        }
      );
      this.loadProfileChartData.datasets.push(
        {
          data: this.data.houses[1].loadProfile,
          label: "Desna hiša",
          cubicInterpolationMode: 'monotone',
          tension: 0.9
          // borderRadius: 2
        }
      );
      this.loadProfileChartData = JSON.parse(JSON.stringify(this.loadProfileChartData));

      // Set solar chart
      this.solarChartData.labels = this.data.timestamps;
      this.solarChartData.datasets = [
        {
          data: this.data.solar[0].pvGen,
          label: "PV generation [W]",
          cubicInterpolationMode: 'monotone',
          tension: 0.9,
          borderColor: "#e6c212",
          backgroundColor: "#FFF"
        }
      ];
      this.solarChartData = JSON.parse(JSON.stringify(this.solarChartData));

      this.subscribeToFirebase();
    }, 1000);
  }

  // Chart.js
  public loadProfileChartOptions: ChartConfiguration['options'] = {
    responsive: true,
    maintainAspectRatio: false,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      x: {
        grid: {
          display: false
        },
        // stacked: true,
        ticks: {
          autoSkip: true,
          maxTicksLimit: 8,
          maxRotation: 0,
          minRotation: 0,
          // color: "#d1d1d1"
        },
      },
      y: {
        beginAtZero: true,
        max: 5000,
        grid: {
          display: false
        },
        // stacked: true,
        // ticks: {
        //   color: "#d1d1d1"
        // }
      },

    },
    plugins: {
      legend: {
        display: true
      }
    },
    elements: {
      point: {
        radius: 0
      }
    }
  };
  public loadProfileChartType: ChartType = 'line';

  public loadProfileChartData: ChartData<'line'> = {
    labels: [],
    datasets: [
      { data: []}
    ]
  };

  // Solar chart
  public solarChartOptions: ChartConfiguration['options'] = {
    responsive: true,
    maintainAspectRatio: false,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      x: {
        grid: {
          display: false
        },
        stacked: true,
        ticks: {
          autoSkip: true,
          maxTicksLimit: 8,
          maxRotation: 0,
          minRotation: 0,
          // color: "#d1d1d1"
        },
      },
      y: {
        grid: {
          display: false
        },
        beginAtZero: true,
        max: 8000,
        // ticks: {
        //   color: "#d1d1d1"
        // }
      },

    },
    plugins: {
      legend: {
        display: true
      }
    },
    elements: {
      point: {
        radius: 0
      }
    }
  };
  public solarChartType: ChartType = 'line';

  public solarChartData: ChartData<'line'> = {
    labels: [],
    datasets: []
  };

  /**
   * Subscribe and listen to changes about load profiles, weather and time
   */
  subscribeToFirebase() {
    // Firebase
    const refHouse0 = this.db.object("house0");
    refHouse0.valueChanges().subscribe((data) => {
      console.log("house0:", data);
      this.updateHouse(0, data as string);
    });
    const refHouse1 = this.db.object("house1");
    refHouse1.valueChanges().subscribe((data) => {
      console.log("house1:", data);
      this.updateHouse(1, data as string);
      // this.data.time = data as string;
    });
    const refSolar = this.db.object("house1weather");
    refSolar.valueChanges().subscribe((data) => {
      console.log("house1weather:", data);
      this.updateWeather(1, data as string);
    });
    const refBattery = this.db.object("house1battery");
    refBattery.valueChanges().subscribe((data) => {
      console.log("house1battery:", data);
      this.setBattery(1, data as boolean);
    });

    // Listen for current time
    const ref = this.db.object("time");
    ref.valueChanges().subscribe((data) => {
      console.log("Time:", data);
      this.data.time = data as string;
    })
  }

  /**
   * Update chart of house load profiles
   * @param houseIndex
   * @param profileName
   */
  updateHouse(houseIndex: number, profileName: string) {
    this.houseLabels[houseIndex] = profileName;
    let profileIndex: number = 0;
    this.data.houses.some((house: House, index: number) => {
      if (house.name === profileName) {
        profileIndex = index;
        return true;
      }
      return false;
    });
    this.loadProfileChartData.datasets[houseIndex].data =
      this.data.houses[profileIndex]?.loadProfile;

    this.charts?.forEach((child) => {
      child.chart?.update()
    });
  }

  /**
   * Select load profile type of the selected house
   * @param houseIndex
   * @param profileName
   */
  selectProfile(houseIndex: number, profileName: string) {
    this.updateHouse(houseIndex, profileName);

    const ref = this.db.object("house" + houseIndex);
    ref.set(profileName).then((resp) => {
      // console.log("Response: ", resp);
    }).catch((error) => {
      console.log("Error: ", error);
    });
  }

  /**
   * Update chart of solar energy generation in selected weather
   * @param houseIndex
   * @param profileName
   */
  updateWeather(houseIndex: number, profileName: string) {
    this.weatherLabel = profileName;
    let profileIndex: number = 0;
    this.data.solar.some((solar: House, index: number) => {
      if (solar.name === profileName) {
        profileIndex = index;
        return true;
      }
      return false;
    });
    this.solarChartData.datasets[0].data =
      this.data.solar[profileIndex]?.pvGen;

    this.charts?.forEach((child) => {
      child.chart?.update()
    });
  }

  /**
   * Select weather type
   * @param houseIndex
   * @param profileName
   */
  selectWeather(houseIndex: number, profileName: string) {
    this.updateWeather(houseIndex, profileName);

    const ref = this.db.object("house" + houseIndex + "weather");
    ref.set(profileName).then((resp) => {
      // console.log("Response: ", resp);
    }).catch((error) => {
      console.log("Error: ", error);
    });
  }

  /**
   * Set battery
   * @param houseIndex
   * @param useBattery
   */
  setBattery(houseIndex: number, useBattery: boolean) {
    this.useBattery = useBattery;
    const ref = this.db.object("house" + houseIndex + "battery");
    ref.set(useBattery).then((resp) => {
      // console.log("Response: ", resp);
    }).catch((error) => {
      console.log("Error: ", error);
    });
  }

  selectTab(tabIndex: number) {
    this.selectedTab = tabIndex;
  }

  endExplore() {
    const ref = this.db.object("scenario");
    ref.set("none").then((resp) => {
    }).catch((error) => {
      console.log("Error: ", error);
    });
    this.router.navigate(["/dashboard-home"]);
  }

}
