import {Component} from "@angular/core";
import {AngularFireDatabase} from "@angular/fire/compat/database";

@Component({
  selector: 'ambient-time',
  templateUrl: './ambient-time.html',
  styleUrls: ['./ambient-time.scss'],
})
export class AmbientTime {

  timeHour: number = 10;

  constructor(
    private db: AngularFireDatabase
  ) {


    const ref = this.db.list("questionnaire-tst");
    ref.valueChanges().subscribe((data) => {
      console.log(data);
      data.forEach((el) => {
        // @ts-ignore
        console.log(new Date(el.timestamp), " - ", el.radioAns);
      });
    });

  }

  setCurrentTime() {
    console.log("Set current time");

    const ref = this.db.object("ambient-time");
    ref.set(-12).then((resp) => {}).catch((error) => {
      console.log("Error: ", error);
    });
  }

  setTime() {
    console.log("Set time:", this.timeHour);

    if (this.timeHour < 0) {
      this.timeHour = 0;
    } else if (this.timeHour > 23) {
      this.timeHour = 23;
    }

    const ref = this.db.object("ambient-time");
    ref.set(this.timeHour).then((resp) => {}).catch((error) => {
      console.log("Error: ", error);
    });
  }

  minus() {
    if (this.timeHour > 0) {
      this.timeHour--;
    } else {
      this.timeHour = 23;
    }
    this.setTime();
  }

  plus() {
    if (this.timeHour < 22) {
      this.timeHour++;
    } else {
      this.timeHour = 0;
    }
    this.setTime();
  }
}
