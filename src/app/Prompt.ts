

export class Prompt {
  id: string = "";
  message: string = "";
  emoji: string = "";
  over: boolean = false;
  equal: boolean = false;
  under: boolean = false;

  title: string = "";
  text: string = "";
}
