import {Component, QueryList, ViewChildren} from "@angular/core";
import {BaseChartDirective} from "ng2-charts";
import {House} from "../House";
import {HttpClient} from "@angular/common/http";
import {Appliance} from "../Appliance";
import {Chart, ChartConfiguration, ChartData, ChartType} from "chart.js";
import {Data} from "../models/data";
import {AngularFireDatabase} from "@angular/fire/compat/database";
import {Router} from "@angular/router";
import {Prompt} from "../Prompt";

@Component({
  selector: 'main-component',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent {
  // @ViewChild(BaseChartDirective) chart: BaseChartDirective | undefined;
  @ViewChildren(BaseChartDirective) charts: QueryList<BaseChartDirective> | undefined;

  public data: Data;
  public houses: House[] = [];

  time: string = 'Waiting...';
  timestamps: string[] = [];

  currentWeather: string = "SUNNY";

  currentIndex: number = 0;

  solarIndex: number = 0;
  currentScenario: string = "none";

  updateInfoTimeout: any;
  showCover: boolean = true;

  colorGrid: string = "#cb762d";
  colorGreen: string = "#7ecb40";

  ambientTime: number = -12;

  scalesRotation: number = 0;
  weightMargin: number = 40;

  currentPrompt: Prompt = new Prompt();
  currentPromptIndex: number = 0;
  promptOpacity: number = 0;
  ticksSince: number = 0;
  questionnaireUrl: string = "";

  answerCounter: number = 0;
  upOpacity: number = 0;

  constructor(
    private http: HttpClient,
    private db: AngularFireDatabase,
    private router: Router) {
    this.data = new Data(this.http);
    this.data.getHouseData('assets/archetypes_exp.json');
    this.data.getSolarData('assets/pv_gen.json');
    this.data.getPromptsData('assets/prompts.json', () => {});
    setTimeout(() => {
      this.timestamps = this.data.timestamps;
      // this.houses = this.data.houses;
      this.houses[0] = JSON.parse(JSON.stringify(this.data.houses[0]));
      this.houses[1] = JSON.parse(JSON.stringify(this.data.houses[4]));
      this.houses[2] = JSON.parse(JSON.stringify(this.data.houses[2]));

      this.houses = this.houses.slice(0, 3);

      // Solar
      this.houses[1].pvGen = this.data.solar[0].pvGen;
      console.log("Houses with set SOLAR:", this.houses);

      this.houses.forEach((house: House) => {
        house.battery_capacity = 0;
      });

      // Coordinate
      for (let i = 0; i < this.timestamps.length; i++) {
        this.data.coordinateExisting(i, this.houses, false);
      }
      this.data.coordinateCommunity(this.houses, this.timestamps);
      console.log("Community data:", this.data.community);

      // Update chart
      this.barChartData.labels = this.timestamps;
      this.barChartData.datasets = [];
      // for (const house of this.houses) {
      //   this.barChartData.datasets.push(
      //     {
      //       data: house.loadProfile,
      //       label: house.name,
      //       //cubicInterpolationMode: 'monotone',
      //       //tension: 0.9
      //       borderRadius: 2,
      //       backgroundColor: house.color
      //     }
      //   )
      // }
      const housesChartData: House[] = JSON.parse(JSON.stringify(this.houses));
      housesChartData.forEach((house: House) => {
        house.loadProfile.forEach((load: number, i: number) => {
          house.loadProfile[i] = load / 1000;
          house.pvGen[i] = house.pvGen[i] / 1000;
        });
      });

      this.barChartData.datasets.push(
        {
          data: housesChartData[0].loadProfile,
          label: "Rumena hiša",
          borderRadius: 2,
          backgroundColor: "#cccc62BB"
        }
      );
      this.barChartData.datasets.push(
        {
          data: housesChartData[1].loadProfile,
          label: "Rdeča - šola",
          borderRadius: 2,
          backgroundColor: "#cc6d62BB"
        }
      );
      this.barChartData.datasets.push(
        {
          data: housesChartData[2].loadProfile,
          label: "Modra hiša",
          borderRadius: 2,
          backgroundColor: "#6299ccBB"
        }
      );
      // Add PV data
      this.barChartData.datasets.push(
        {
          data: housesChartData[1].pvGen,
          label: "Proizvodnja energije iz sončne elektrarne",
          type: "line",
          cubicInterpolationMode: 'monotone',
          tension: 0.9,
          borderColor: this.colorGreen,
          backgroundColor: "#000",
          borderWidth: 5
        }
      );
      Chart.defaults.color = "#d1d1d1";
      // this.chart?.update();
      this.barChartData = JSON.parse(JSON.stringify(this.barChartData));

      this.updateGraph(30);

      this.subscribeToFirebase();

      setTimeout(() => {
        this.showCover = false;
      }, 100);
    }, 1000);


  }

  subscribeToFirebase() {
    // Firebase
    const ref = this.db.object("scenario");
    ref.valueChanges().subscribe((data) => {
      console.log("scenario:", data);
      const newScenario: string = data as string;
      if (newScenario !== this.currentScenario && newScenario !== "none") {
        this.showCover = true;
        clearTimeout(this.updateInfoTimeout);
        setTimeout(() => {
          this.router.navigate(["/tutorial"]);
        }, 2000);
      }
    });

    const refTime = this.db.object("ambient-time");
    refTime.valueChanges().subscribe((data) => {
      this.ambientTime = data as number;
    });

    const refAnswers = this.db.list("questionnaire-tst");
    refAnswers.valueChanges().subscribe((data) => {
      console.log("Answers change:", data.length);
      this.answerCounter = data.length;
      this.upOpacity = 1;
      setTimeout(() => {
        this.upOpacity = 0;
      }, 1000);
    });
  }

  updateGraph(index: number): void {
    // this.time = this.timestamps[index];
    let d = new Date(); // for now
    if (this.ambientTime !== -12) {
      d.setHours(this.ambientTime);
      d.setMinutes(0);
    }

    this.time = d.toLocaleTimeString([], {hour: '2-digit', minute: '2-digit'});

    // Set index according to current time
    index = Math.floor(d.getHours() * 4 + d.getMinutes() / 15);
    this.currentIndex = index;
    console.log("Calcualted index:", index);

    // Update chart
    this.barChartDataHouse1.datasets[0].data[0] = this.houses[0]?.load_grid[index] / 1000;
    this.barChartDataHouse1.datasets[1].data[0] = this.houses[0]?.load_community[index] / 1000;

    this.barChartDataHouse2.datasets[0].data[0] = this.houses[1]?.load_grid[index] / 1000;
    this.barChartDataHouse2.datasets[1].data[0] = this.houses[1]?.load_community[index] / 1000;
    this.barChartDataHouse2.datasets[2].data[0] = this.houses[1]?.load_pv[index] / 1000;
    this.barChartDataHouse2.datasets[3].data[1] = this.houses[1]?.to_community[index] / 1000;

    this.barChartDataHouse3.datasets[0].data[0] = this.houses[2]?.load_grid[index] / 1000;
    this.barChartDataHouse3.datasets[1].data[0] = this.houses[2]?.load_community[index] / 1000;

    this.barChartDataHouse4.datasets[0].data[0] = this.houses[3]?.load_grid[index] / 1000;
    this.barChartDataHouse4.datasets[1].data[0] = this.houses[3]?.load_community[index] / 1000;

    this.chartDataSelfConsumption.datasets[0].data[0] = this.data.community.selfConsumption[index];
    this.chartDataSelfConsumption.datasets[0].data[1] =  1 - this.data.community.selfConsumption[index];
    this.chartDataSelfConsumption.datasets[1].data[0] = this.data.community.daySelfConsumption;
    this.chartDataSelfConsumption.datasets[1].data[1] =  1 - this.data.community.daySelfConsumption;

    this.chartDataSelfSufficiency.datasets[0].data[0] = this.data.community.selfSufficiency[index];
    this.chartDataSelfSufficiency.datasets[0].data[1] =  1 - this.data.community.selfSufficiency[index];
    this.chartDataSelfSufficiency.datasets[1].data[0] = this.data.community.daySelfSufficiency;
    this.chartDataSelfSufficiency.datasets[1].data[1] =  1 - this.data.community.daySelfSufficiency;

    let rotation: number =
      (this.data.community.pvGen[index] - this.data.community.consumption[index]) / 100;
    if (rotation > 30) {
      rotation = 30;
    } else if (rotation < -30) {
      rotation = -30;
    }
    this.scalesRotation = rotation;
    this.weightMargin = 40 + rotation * 1.4;

    this.charts?.forEach((child) => {
      child.chart?.update()
    });

    // Prompt
    this.ticksSince++;
    if (this.promptOpacity === 0) {
      if (this.ticksSince === 5) {
        this.promptOpacity = 1;
        this.ticksSince = 0;

        let stop: boolean = false;
        let nextIndex: number = this.currentPromptIndex + 1;
        while(!stop) {

          if (nextIndex > this.data.prompts.length - 1) {
            nextIndex = 0;
          }
          // console.log(this.scalesRotation);
          // console.log(this.data.prompts[nextIndex]);
          // console.log(this.data.prompts[nextIndex].over);
          if (this.scalesRotation > 10) {
            if (this.data.prompts[nextIndex].over) {
              stop = true;
            }
          } else if (this.scalesRotation < -10) {
            if (this.data.prompts[nextIndex].under) {
              stop = true;
            }
          } else {
            if (this.data.prompts[nextIndex].equal) {
              stop = true;
            }
          }
          if (stop) {
            this.currentPromptIndex = nextIndex;
            this.currentPrompt = this.data.prompts[this.currentPromptIndex];
            this.questionnaireUrl =
              "https://skupnost-v1.web.app/questionnaire?id=" + this.currentPrompt.id;
          } else {
            nextIndex++;
          }
          // stop = true;
        }
      }
    } else {
      if (this.ticksSince === 60) {
        this.promptOpacity = 0;
        this.ticksSince = 0;
      }
    }

    if (index < this.timestamps.length - 1) {
      this.updateInfoTimeout = setTimeout(() => {
        this.updateGraph(index + 1);
      }, 1000);
    } else {
      this.updateInfoTimeout = setTimeout(() => {
        this.updateGraph(0);
      }, 1000);
    }
  }

  getHeight(value: number | undefined, ratio: number = 1): string {
    if (value === undefined) {
      return "0%";
    }
    let height: number = value >= 0 ? (value / ratio) * 100 : 0;
    if (height > 100) {
      height = 100;
    }
    return height + '%';
  }

  coordinate(index: number) {
    // console.log("-------------------------");
    // console.log("Coordinate for tick ", index);

    let communityOverGen: number = 0;
    this.houses.forEach((house: House) => {
      // Set agent own values
      if (house.pvGen.length > 0) {
        house.load_pv[index] =
          house.pvGen[index] >= house.loadProfile[index] ?
            house.loadProfile[index] :
            house.pvGen[index];
      }
      // Calculate how much community generated energy is available to other agents
      const agentOverGen = house.pvGen[index] - house.loadProfile[index];
      house.to_community[index] = agentOverGen > 0 ? agentOverGen : 0; // TODO Split on community and grid?
      communityOverGen += house.to_community[index];
    });
    // console.log("Community over gen:", communityOverGen);

    // Distribute available community energy
    this.houses.forEach((house: House) => {
      let agentEnergyNeed = house.loadProfile[index] - house.pvGen[index];
      if (agentEnergyNeed > 0) {
        if (communityOverGen > 0) {
          if (communityOverGen > agentEnergyNeed) {
            house.load_community[index] = agentEnergyNeed;
            communityOverGen -= agentEnergyNeed;
            agentEnergyNeed = 0;
          } else {
            house.load_community[index] = communityOverGen;
            agentEnergyNeed -= communityOverGen;
            communityOverGen = 0;
          }
        }
      }
      house.load_grid[index] = agentEnergyNeed > 0 ? agentEnergyNeed : 0;
    });
  }

  /**
   Weather settings
   */
  changeWeather() {
    console.log("Change weather");

    // if (this.currentWeather === "SUNNY") {
    //   this.currentWeather = "CLOUDY"
    //   this.allAgents.forEach((agent: Agent[]) => {
    //     agent.forEach((tick: Agent) => {
    //       tick.pv_gen /= 3;
    //     });
    //   });
    // } else {
    //   this.currentWeather = "SUNNY"
    //   this.allAgents.forEach((agent: Agent[]) => {
    //     agent.forEach((tick: Agent) => {
    //       tick.pv_gen *= 3;
    //     });
    //   });
    // }
  }

  // Chart.js
  public barChartOptions: ChartConfiguration['options'] = {
    responsive: true,
    maintainAspectRatio: false,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      x: {
        grid: {
          display: false
        },
        stacked: true,
        ticks: {
          autoSkip: true,
          maxTicksLimit: 12.4,
          maxRotation: 0,
          minRotation: 0,
          color: "#d1d1d1",
          font: {
            size: 14
          }
        },
      },
      y: {
        grid: {
          display: false
        },
        stacked: true,
        ticks: {
          color: "#d1d1d1",
          font: {
            size: 14
          }
        },
        title: {
          display: true,
          text: "[kW]",
          font: {
            size: 14
          }
        }
      },

    },
    plugins: {
      legend: {
        display: true,
        labels: {
          font: {
            size: 16
          }
        }
      }
    },
    elements: {
      point: {
        radius: 0
      }
    }
  };
  public barChartType: ChartType = 'bar';

  public barChartData: ChartData<'bar' | 'line'> = {
    labels: [],
    datasets: [
      { data: [] }
    ]
  };

  // Chart house 1
  public barChartOptionsHouse1: ChartConfiguration['options'] = {
    responsive: true,
    maintainAspectRatio: false,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      x: {
        grid: {
          display: false
        },
        stacked: true,
        ticks: {
          autoSkip: true,
          maxTicksLimit: 12,
          maxRotation: 0,
          minRotation: 0,
          color: "#d1d1d1",
          font: {
            size: 14
          }
        },
      },
      y: {
        grid: {
          display: false
        },
        stacked: true,
        ticks: {
          color: "#d1d1d1",
          font: {
            size: 14
          }
        },
        min: 0,
        max: 6
      },

    },
    plugins: {
      legend: {
        display: false
      }
    },
    elements: {
      point: {
        radius: 0
      }
    }
  };
  public barChartTypeHouse1: ChartType = 'bar';

  public barChartDataHouse1: ChartData<'bar'> = {
    labels: [ "Poraba [kW]" ],
    datasets: [
      { data: [ 0 ],
        backgroundColor: [this.colorGrid] },
      { data: [ 0 ],
        backgroundColor: [this.colorGreen]}
    ]
  };

  // Chart house 2
  public barChartOptionsHouse2: ChartConfiguration['options'] = this.barChartOptionsHouse1;
  public barChartTypeHouse2: ChartType = 'bar';

  public barChartDataHouse2: ChartData<'bar'> = {
    labels: [ "Poraba [kW]", "Presežek [kW]" ],
    datasets: [
      { data: [ 0, 0 ],
        backgroundColor: [this.colorGrid, "#cb00ae"]},
      { data: [ 0, 0 ],
        backgroundColor: [this.colorGreen, "#cb00ae"]},
      { data: [ 0, 0 ],
        backgroundColor: [this.colorGreen, "#cb00ae"]},
      { data: [ 0, 0 ],
        backgroundColor: [this.colorGrid, this.colorGreen]}
    ]
  };

  // Chart house 3
  public barChartOptionsHouse3: ChartConfiguration['options'] = this.barChartOptionsHouse1;
  public barChartTypeHouse3: ChartType = 'bar';

  public barChartDataHouse3: ChartData<'bar'> = {
    labels: [ "Poraba [kW]" ],
    datasets: [
      { data: [ 0 ],
        backgroundColor: [this.colorGrid] },
      { data: [ 0 ],
        backgroundColor: [this.colorGreen]}
    ]
  };

  // Chart house 4
  public barChartOptionsHouse4: ChartConfiguration['options'] = this.barChartOptionsHouse1;
  public barChartTypeHouse4: ChartType = 'bar';

  public barChartDataHouse4: ChartData<'bar'> = {
    labels: [ "Poraba [kW]" ],
    datasets: [
      { data: [ 0 ],
        backgroundColor: [this.colorGrid] },
      { data: [ 0 ],
        backgroundColor: [this.colorGreen]}
    ]
  };

  // Self
  public chartOptionsSelfConsumption: ChartConfiguration['options'] = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: true,
        labels: {
          font: {
            size: 16
          }
        }
      }
    }
  };
  public chartTypeSelfConsumption: ChartType = 'doughnut';

  public chartDataSelfConsumption: ChartData<'doughnut'> = {
    labels: [],
    datasets: [
      { data: [0, 1],
        rotation: 180,
        borderWidth: 1,
        backgroundColor: ["#69acff", "#a6a6a6"]},
      { data: [0, 1],
        rotation: 180,
        borderWidth: 1,
        backgroundColor: ["#177fff", "#a6a6a6"]}
    ]

  };

  //--
  // Self
  public chartOptionsSelfSufficiency: ChartConfiguration['options'] = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: true,
        labels: {
          font: {
            size: 16
          }
        }
      }
    }
  };
  public chartTypeSelfSufficiency: ChartType = 'doughnut';

  public chartDataSelfSufficiency: ChartData<'doughnut'> = {
    datasets: [
      { data: [0, 1],
        rotation: 180,
        borderWidth: 1,
        backgroundColor: ["#9966ff", "#a6a6a6"]},
      { data: [0, 1],
        rotation: 180,
        borderWidth: 1,
        backgroundColor: ["#6417ff", "#a6a6a6"]}
    ]
  };
}

export class Agent {
  // Agent state
  load: number;
  pv_gen: number;

  // Results from community coordinator
  load_grid: number = 0;
  load_community: number = 0;
  load_pv: number = 0;
  to_community: number = 0;
  to_grid: number = 0;

  constructor(
    load: number,
    pv_gen: number
  ) {
    this.load = load;
    this.pv_gen = pv_gen;
  }
}

/*
export class Agent {
  id: string;
  profile: Profile[];
  decisions: Decision[];

  battery: Battery;
  car: Car;

  // For labels in graphs
  max_load: number;
  max_gen: number
}

export class Profile {
  // Agent state
  load: number;
  pv_gen: number;
}

export class Decision {
  // Results from community coordinator
  load_grid: number = 0;
  load_community: number = 0;
  load_pv: number = 0;
  to_community: number = 0;
  to_grid: number = 0;
}

export class Battery {
  capacity: number;
  state_of_charge: number;
}

export class Car {
  capacity: number;
  state_of_charge: number;
  connected: boolean;
  min_state_of_charge: number; // Minimum charge to allow V2G
}
*/
