import {Component} from "@angular/core";
import {AngularFireDatabase} from "@angular/fire/compat/database";
import {ActivatedRoute} from "@angular/router";
import {Data} from "../models/data";
import {HttpClient} from "@angular/common/http";
import {Prompt} from "../Prompt";

@Component({
  selector: 'ambient-time',
  templateUrl: './questionnaire.html',
  styleUrls: ['./questionnaire.scss'],
})
export class Questionnaire {

  public data: Data;

  scanPrompt: Prompt = new Prompt();

  sent: boolean = false;

  radioAns: number[] = [-1, -1, -1, -1, -1];

  komentar: string = "";

  constructor(
    private http: HttpClient,
    private db: AngularFireDatabase,
    private route: ActivatedRoute
  ) {
    let idParam: string | null = this.route.snapshot.queryParamMap.get("id");
    if (idParam === null) {
      idParam = "default";
    }
    console.log("ID param:", idParam);

    this.data = new Data(this.http);
    this.data.getPromptsData(
      'assets/prompts.json',
      () => {
        console.log("Data:", this.data.prompts);

        for (let data of this.data.prompts) {
          if (data.id === idParam) {
            this.scanPrompt = data;
            break;
          }
        }
      });

  }

  radioClick(questIndex: number, answerIndex: number) {
    this.radioAns[questIndex] = answerIndex;
  }

  send() {
    this.sent = true;
    console.log("send");
    console.log(this.radioAns);
    console.log(this.komentar);

    const answer: any = {
      timestamp: +(new Date()),
      radioAns: this.radioAns,
      comment: this.komentar
    };

    const ref = this.db.list("questionnaire-tst");
    ref.push(answer).then((resp) => {
      console.log("Response: ", resp);
    }).catch((error) => {
      console.log("Error: ", error);
    });
  }
}
