import {Component} from "@angular/core";
import {AngularFireDatabase} from "@angular/fire/compat/database";
import {Router} from "@angular/router";

@Component({
  selector: 'dashboard-home',
  templateUrl: './dashboard-home.html',
  styleUrls: ['./dashboard-home.scss'],
})
export class DashboardHome {
  cardExpanded: boolean[] = [false, false, false, false];

  currentScenario: string = "none";

  constructor(
    private db: AngularFireDatabase,
    private router: Router
  ) {
    this.subscribeToFirebase();
  }

  subscribeToFirebase() {
    // Firebase
    const ref = this.db.object("scenario");
    ref.valueChanges().subscribe((data) => {
      console.log("scenario:", data);
      this.currentScenario = data as string;
    });
  }

  toggleCard(index: number) {
    this.cardExpanded[index] = !this.cardExpanded[index];
  }

  toggleScenario(id: string) {
    if (this.currentScenario === "none") {
      this.currentScenario = id;
      if (id === "explore") {
        this.router.navigate(["/tutorial-dashboard"]);
      }
    } else if (this.currentScenario === id) {
      this.currentScenario = "none";
    }
    const ref = this.db.object("scenario");
    ref.set(this.currentScenario).then((resp) => {
    }).catch((error) => {
      console.log("Error: ", error);
    });
  }
}
