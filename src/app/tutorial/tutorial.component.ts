import {Component, ElementRef, QueryList, Renderer2, ViewChild, ViewChildren} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Data} from "../models/data";
import {House} from "../House";
import {AngularFireDatabase} from "@angular/fire/compat/database";
import {Narration} from "../models/narration";
import {Chart, ChartConfiguration, ChartData, ChartType} from "chart.js";
import {BaseChartDirective} from "ng2-charts";
import * as $ from 'jquery';
import {Router} from "@angular/router";

@Component({
  selector: 'tutorial-component',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.scss'],
})
export class TutorialComponent {
  @ViewChild('frontRow') frontRow!: ElementRef;
  @ViewChild('backRow') backRow!: ElementRef;

  dotsHouse1: number[] = [1, 1, 1, 0, 0, 0, 0, 0];
  dotsHouse1Color: string = "white";
  dotsHouse2: number[] = [0, 0, 1, 1, 1, 0, 0, 0];
  dotsHouse2Color: string = "white";

  house1color = "rgba(39,140,255,0.7)";
  house2color = "rgba(255,74,82,0.7)";

  // // 1
  // colorGrid: string = "#F9C416";
  // colorGreen: string = "#84BC41";
  // // 2
  // colorGrid: string = "#F47A2B";
  // colorGreen: string = "#008F4D";
  // // 3
  // colorGrid: string = "#5D5E5E";
  // colorGreen: string = "#F5C344";
  // 4
  colorGrid: string = "#cb762d";
  colorGreen: string = "#7ecb40";
  // // 5
  // colorGrid: string = "#DA4E38";
  // colorGreen: string = "#34C687";

  public data: Data;
  time: string = 'Waiting...';
  timestamps: string[] = [];
  public houses: House[] = [];
  currentIndex: number = 0;

  pauseAnim: boolean = false;
  showNarration: boolean = false;
  showKW: boolean = false;
  showTimeline: boolean = true;

  useBattery: boolean = true;

  noCommunity: boolean = false;
  hideProfileName: boolean = false;

  lineLength: number = 8;
  highlightLength: number = 3;
  lineHouse1Pos: number = 0;
  lineHouse2Pos: number = 0;

  directionHouse1: boolean = true;
  delayHouse1: number = 100;
  house1AnimDuration: number = 0;
  lineHouse1: Line = new Line("house1-grid", this.house1AnimDuration, this.directionHouse1);
  lineSolar1: Line = new Line("solar-house-1", 0, false);
  lineGrid1: Line = new Line("solar-grid-1", 0, false);
  lineSolarBattery1: Line = new Line("solar-battery-1", 0, false);
  lineBatteryHouse1: Line = new Line("battery-house-1", 0, false);

  directionHouse2: boolean = true;
  delayHouse2: number = 100;
  house2AnimDuration: number = 3;
  lineHouse2: Line = new Line("house2-grid");
  lineHouse2fromCommunity: Line = new Line("house2-community");

  narration = new Narration();

  scalesRotation: number = 0;
  weightMargin: number = 40;

  narrationArray = [
    {
      content: "Obe hiši vso elektriko pridobivata iz omrežja.",
      startTime: 2,
      endTime: 3,
      top: "78%",
      left: "11%",
      width: "20%",
      height: "6%",
      tick: "right-tick"
    },
    {
      content: "Odjem elektrike iz omrežja je označen z oranžno barvo.",
      startTime: 3.5,
      endTime: 4.5,
      top: "74%",
      left: "9%",
      width: "20%",
      height: "6%",
      tick: "right-tick"
    },
    {
      content: "Ena hiša ima na strehi nameščeno sončno elektrarno.",
      startTime: 5,
      endTime: 6,
      top: "7%",
      left: "50%",
      width: "22%",
      height: "6%",
      tick: "bottom-right-tick"
    },
    {
      content: "Ob sončnem vzhodu sončni paneli začnejo generirati elektriko.",
      startTime: 7.25,
      endTime: 8.25,
      top: "7%",
      left: "50%",
      width: "22%",
      height: "6%",
      tick: "bottom-right-tick"
    },
    {
      content: "Kmalu je proizvedene elektrike dovolj za celotno porabo naprav v hiši.",
      startTime: 8.75,
      endTime: 9.75,
      top: "65%",
      left: "71%",
      width: "22%",
      height: "8%",
      tick: "left-tick"
    },
    {
      content: "Presežek proizvedene energije je oddan v omrežje.",
      startTime: 10.25,
      endTime: 11.25,
      top: "42%",
      left: "32%",
      width: "20%",
      height: "6%",
      tick: "bottom-right-tick"
    },
    {
      content: "Presežek proizvedene zelene energije lahko odkupi sosed s katerim sta v energetski skupnosti.",
      startTime: 12.75,
      endTime: 14.25,
      top: "73%",
      left: "8%",
      width: "22%",
      height: "10%",
      tick: "right-tick"
    },
    {
      content: "Pretok zelene energije je označen z zeleno barvo.",
      startTime: 14.75,
      endTime: 15.75,
      top: "74%",
      left: "9%",
      width: "20%",
      height: "6%",
      tick: "right-tick"
    },
    {
      content: "Z lokalno porabo proizvedene elektrike se zmanjša obremenitev omrežja.",
      startTime: 16.25,
      endTime: 17.25,
      top: "78%",
      left: "11%",
      width: "20%",
      height: "8%",
      tick: "right-tick"
    },
    {
      content: "Zvečer se zmanjša količina proizvedene elektrike.",
      startTime: 18,
      endTime: 19,
      top: "7%",
      left: "50%",
      width: "22%",
      height: "6%",
      tick: "bottom-right-tick"
    },
    {
      content: "Hiši elektriko ponovno pridobivata iz omrežja.",
      startTime: 20,
      endTime: 21,
      top: "78%",
      left: "11%",
      width: "20%",
      height: "6%",
      tick: "right-tick"
    },
  ];

  solarIndex: number = 1;
  solarIndexes: number[] = [0, 1, 2];

  @ViewChildren(BaseChartDirective) charts: QueryList<BaseChartDirective> | undefined;

  visualizationType: number = 0;

  updateInfoTimeout: any;
  currentScenario: string = "none";

  showCover: boolean = true;

  constructor(
    private http: HttpClient,
    private db: AngularFireDatabase,
    private renderer2: Renderer2,
    private router: Router) {

    this.data = new Data(this.http);
    this.data.getHouseData('assets/archetypes_exp.json');
    this.data.getSolarData('assets/pv_gen.json');
    setTimeout(() => {
      this.timestamps = this.data.timestamps;
      this.houses = JSON.parse(JSON.stringify(this.data.houses));

      console.log("Houses data:", this.data.houses);
      this.houses[0] = JSON.parse(JSON.stringify(this.data.houses[3]));
      this.houses[1] = JSON.parse(JSON.stringify(this.data.houses[1]));

      // Solar
      this.houses[1].pvGen = this.data.solar[0].pvGen;
      console.log("Houses with set SOLAR:", this.houses);

      // Firebase
      const refHouse0 = this.db.object("house0");
      refHouse0.valueChanges().subscribe((data) => {
        console.log("house0:", data);
        this.updateHouse(0, data as string);
      });
      const refHouse1 = this.db.object("house1");
      refHouse1.valueChanges().subscribe((data) => {
        console.log("house1:", data);
        this.updateHouse(1, data as string);
        // this.data.time = data as string;
      });
      const refSolar = this.db.object("house1weather");
      refSolar.valueChanges().subscribe((data) => {
        console.log("house1weather:", data);
        this.updateWeather(1, data as string);
      });
      const refBattery = this.db.object("house1battery");
      refBattery.valueChanges().subscribe((data) => {
        console.log("house1battery:", data);
        this.updateBattery(1, data as boolean);
      });

      // Scenarios
      const ref = this.db.object("scenario");
      ref.valueChanges().subscribe((data) => {
        console.log("scenario:", data);
        const newScenario: string = data as string;
        if (newScenario !== this.currentScenario && newScenario === "none") {
          clearTimeout(this.updateInfoTimeout);
          clearTimeout(this.lineHouse1.timeout);
          clearTimeout(this.lineSolar1.timeout);
          clearTimeout(this.lineGrid1.timeout);
          clearTimeout(this.lineSolarBattery1.timeout);
          clearTimeout(this.lineBatteryHouse1.timeout);
          clearTimeout(this.lineHouse2.timeout);
          clearTimeout(this.lineHouse2fromCommunity.timeout);
          this.showCover = true;
          setTimeout(() => {
            this.router.navigate(["/prkz"]);
          }, 2000);

        } else if (newScenario !== this.currentScenario && newScenario !== "none") {
          this.currentScenario = newScenario;
          if (newScenario === "scenario1") {
            this.showNarration = true;
            this.noCommunity = true;
            this.hideProfileName = true;
            this.narrationArray = this.narrationScenario1;
            this.setProfile(0, "Coffee maker");
            this.setProfile(1, "Evening user");
            this.setBattery(1, false);
            this.setWeather(1, "Sončno");
          } else if (newScenario === "scenario2") {
            this.showNarration = true;
            this.noCommunity = false;
            this.hideProfileName = true;
            this.narrationArray = this.narrationScenario2;
            this.setProfile(0, "Coffee maker");
            this.setProfile(1, "Evening user");
            this.setBattery(1, false);
            this.setWeather(1, "Sončno");
          } else if (newScenario === "scenario3") {
            this.noCommunity = false;
            this.showNarration = true;
            this.hideProfileName = true;
            this.narrationArray = this.narrationScenario3;
            this.setProfile(0, "Coffee maker");
            this.setProfile(1, "Evening user");
            this.setBattery(1, true);
            this.setWeather(1, "Sončno");
          } else {
            this.noCommunity = false;
            this.showNarration = false;
            this.hideProfileName = false;
            this.setBattery(1, false);
          }
        }
      });

      this.updateInfo(0);

      // Dots
      this.setDotsHouse1(100, false);
      this.setDotsHouse2(150, false);

      // Timeline chart
      this.updateTimelineChart();

      // Rain
      this.makeItRain();

      setTimeout(() => {
        this.showCover = false;
      }, 100);
    }, 1000);

    setTimeout(() => {
      console.log("Set line house ---------------------");
      // this.setLineHouse1(this.house1AnimDuration, "house1-grid");
      this.setLineHouse(this.lineHouse1);
      this.setLineHouse(this.lineSolar1);
      this.setLineHouse(this.lineGrid1);
      this.setLineHouse(this.lineSolarBattery1);
      this.setLineHouse(this.lineBatteryHouse1);
      this.setLineHouse(this.lineHouse2);
      this.setLineHouse(this.lineHouse2fromCommunity);
    }, 100);
  }

  updateInfo(index: number) {
    console.log("update info:", index);
    this.currentIndex = index;

    // Handle day-night transition
    this.handleDayNightTransition();

    // Send time to Firebase
    this.time = this.timestamps[index];

    this.lineHouse1.delay = this.getDelayFromPower(this.houses[1].load_grid[index]);
    this.lineSolar1.delay = this.getDelayFromPower(this.houses[1].load_pv[index]);
    this.lineGrid1.delay = this.getDelayFromPower(this.houses[1].to_community[index]);
    this.lineSolarBattery1.delay = this.getDelayFromPower(this.houses[1].to_battery[index]);
    this.lineBatteryHouse1.delay = this.getDelayFromPower(this.houses[1].load_battery[index]);

    this.lineHouse2.delay = this.getDelayFromPower(this.houses[0].load_grid[index]);
    this.lineHouse2fromCommunity.delay = this.getDelayFromPower(this.houses[0].load_community[index]);

    // Update charts
    this.barChartDataHouse1.datasets[0].data[0] = this.houses[1]?.load_grid[index] / 1000;
    this.barChartDataHouse1.datasets[1].data[0] = this.houses[1]?.load_community[index] / 1000;
    this.barChartDataHouse1.datasets[2].data[0] = this.houses[1]?.load_pv[index] / 1000;
    this.barChartDataHouse1.datasets[3].data[1] = this.houses[1]?.to_community[index] / 1000;

    this.barChartDataHouse2.datasets[0].data[0] = this.houses[0]?.load_grid[index] / 1000;
    this.barChartDataHouse2.datasets[1].data[0] = this.houses[0]?.load_community[index] / 1000;

    this.chartDataSelfConsumption.datasets[0].data[0] = this.data.community.selfConsumption[index];
    this.chartDataSelfConsumption.datasets[0].data[1] =  1 - this.data.community.selfConsumption[index];
    this.chartDataSelfConsumption.datasets[1].data[0] = this.data.community.daySelfConsumption;
    this.chartDataSelfConsumption.datasets[1].data[1] =  1 - this.data.community.daySelfConsumption;

    this.chartDataSelfSufficiency.datasets[0].data[0] = this.data.community.selfSufficiency[index];
    this.chartDataSelfSufficiency.datasets[0].data[1] =  1 - this.data.community.selfSufficiency[index];
    this.chartDataSelfSufficiency.datasets[1].data[0] = this.data.community.daySelfSufficiency;
    this.chartDataSelfSufficiency.datasets[1].data[1] =  1 - this.data.community.daySelfSufficiency;

    this.charts?.forEach((child) => {
      child.chart?.update()
    });

    let rotation: number =
      (this.data.community.pvGen[index] - this.data.community.consumption[index]) / 100;
    if (rotation > 30) {
      rotation = 30;
    } else if (rotation < -30) {
      rotation = -30;
    }
    this.scalesRotation = rotation;
    this.weightMargin = 40 + rotation * 1.4;

    // Set narration
    this.narration.opacity = 0;
    if (this.showNarration) {
      this.narrationArray.forEach((narrationData) => {
        console.log("Narration start time:", narrationData.startTime * 4, " - index:", index);
        if (index >= narrationData.startTime * 4 && index <= narrationData.endTime * 4) {
          this.narration.setNarrationData(narrationData);
        }
      });
    }

    if (!this.pauseAnim) {
      if (index < this.timestamps.length - 1) {
        this.updateInfoTimeout = setTimeout(() => {
          this.updateInfo(index + 1);
        }, 1000);
      } else {
        this.updateInfoTimeout = setTimeout(() => {
          this.updateInfo(0);
        }, 1000);
      }
    }
  }

  handleDayNightTransition() {
    this.solarIndexes.forEach((solarIndex: number) => {
      if (this.currentIndex === 30) {
        const dayBcg = document.getElementById("day-background-" + solarIndex);
        if (dayBcg !== null) {
          dayBcg.style.opacity = "1";
        }
      } else if (this.currentIndex === 74) {
        const dayBcg = document.getElementById("day-background-" + solarIndex);
        if (dayBcg !== null) {
          dayBcg.style.opacity = "0";
        }
      }
    });
  }

  updateDayNightTransition() {
    setTimeout(() => {
      this.solarIndexes.forEach((solarIndex: number) => {
        if (this.currentIndex < 30 || this.currentIndex >= 74) {
          const dayBcg = document.getElementById("day-background-" + solarIndex);
          if (dayBcg !== null) {
            dayBcg.style.opacity = "0";
          }
        } else {
          const dayBcg = document.getElementById("day-background-" + solarIndex);
          if (dayBcg !== null) {
            dayBcg.style.opacity = "1";
          }
        }
      });
    }, 100);

  }

  setDotsHouse1(delay: number, increase: boolean) {
    setTimeout(() => {
      if (increase) {
        if (this.lineHouse1Pos < this.lineLength + this.highlightLength - 1) {
          this.lineHouse1Pos += 1;
        } else {
          this.lineHouse1Pos = 0;
        }
      } else {
        if (this.lineHouse1Pos > 0) {
          this.lineHouse1Pos -= 1;
        } else {
          this.lineHouse1Pos = this.lineLength + this.highlightLength - 1;
        }
      }

      this.dotsHouse1.forEach((dot: number, index: number) => {
        if (index >= this.lineHouse1Pos - this.highlightLength && index < this.lineHouse1Pos) {
          this.dotsHouse1[index] = 1;
        } else {
          this.dotsHouse1[index] = 0;
        }
      });

      if (this.houses[1].load_grid[this.currentIndex] > 0) {
        this.delayHouse1 = this.getDelayFromPower(this.houses[1].load_grid[this.currentIndex]) * 40;
        this.directionHouse1 = false;
        this.dotsHouse1Color = this.colorGrid;
      } else {
        this.delayHouse1 = this.getDelayFromPower(this.houses[1].to_community[this.currentIndex]) * 40;
        this.directionHouse1 = true;
        this.dotsHouse1Color = this.colorGreen;
      }

      if (
        this.houses[1].load_grid[this.currentIndex] === 0 &&
        this.houses[1].load_community[this.currentIndex] === 0 &&
        this.houses[1].to_community[this.currentIndex] === 0) {
        this.dotsHouse1.forEach((dot: number, index: number) => {
          this.dotsHouse1[index] = 0;
        });
      }

      this.setDotsHouse1(this.delayHouse1, this.directionHouse1);
    }, delay);
  }

  setDotsHouse2(delay: number, increase: boolean) {
    setTimeout(() => {
      if (increase) {
        if (this.lineHouse2Pos < this.lineLength + this.highlightLength - 1) {
          this.lineHouse2Pos += 1;
        } else {
          this.lineHouse2Pos = 0;
        }
      } else {
        if (this.lineHouse2Pos > 0) {
          this.lineHouse2Pos -= 1;
        } else {
          this.lineHouse2Pos = this.lineLength + this.highlightLength - 1;
        }
      }
      this.dotsHouse1.forEach((dot: number, index: number) => {
        if (index >= this.lineHouse2Pos - this.highlightLength && index < this.lineHouse2Pos) {
          this.dotsHouse2[index] = 1;
        } else {
          this.dotsHouse2[index] = 0;
        }
      });

      this.delayHouse1 =
        this.getDelayFromPower(
          this.houses[0].load_grid[this.currentIndex] +
          this.houses[0].load_community[this.currentIndex]) * 40;
      if (this.houses[0].load_grid[this.currentIndex] > this.houses[0].load_community[this.currentIndex]) {
        this.dotsHouse2Color = this.colorGrid;
      } else {
        this.dotsHouse2Color = this.colorGreen;
      }

      this.setDotsHouse2(delay, increase);
    }, delay);
  }

  /**
   * Set speed of animated energy line
   * @param line
   */
  setLineHouse(line: Line) {
    // console.log("setLineHouse - ", line.elementId, " - ", (line.delay * 1000));
    const houseEl = document.getElementById(line.elementId);
    setTimeout(() => {
      if (houseEl !== undefined && houseEl !== null && line.delay > 0) {
        houseEl.classList.add(line.direction ? 'forward' : "reverse");
        houseEl.style.animationDuration = this.getStringDuration(line.delay);
        houseEl.style.backgroundColor = line.direction ? this.colorGrid : this.colorGreen;
      }
    }, 100);

    line.timeout = setTimeout(() => {
      if (houseEl !== undefined && houseEl !== null) {
        houseEl.classList.remove('forward');
        houseEl.classList.remove('reverse');
      }

      this.setLineHouse(line);
    }, line.delay * 1000);
  }



  changeDirection() {
    this.lineHouse1.direction = !this.lineHouse1.direction;
  }

  increaseSpeed() {
    this.delayHouse1 -= 10;
    this.lineHouse1.delay -= 0.2;
  }

  decreaseSpeed() {
    this.delayHouse1 += 10;
    this.lineHouse1.delay += 0.2;
  }

  /**
   * Pause the time
   */
  pause() {
    if (this.pauseAnim) {
      this.pauseAnim = false;
      this.updateInfo(this.currentIndex);
    } else {
      this.pauseAnim = true;
    }
  }

  /**
   * Switch visualization
   */
  switchVisualization() {
    if (this.visualizationType <= 1) {
      this.visualizationType ++;
    } else {
      this.visualizationType = 0;
    }
  }

  /**
   * Show or hide the narration
   */
  toggleNarration() {
    this.showNarration = !this.showNarration;
  }

  /**
   * Show or hide energy use and generation in numbers [kW]
   */
  toggleKW() {
    this.showKW = !this.showKW;
  }

  /**
   * Show or hide timeline column at the bottom
   */
  toggleTimeline() {
    this.showTimeline = !this.showTimeline;
  }

  /**
   * Set load profile for selected house
   * @param houseIndex
   * @param selectedProfile
   */
  selectProfile(houseIndex: number, selectedProfile: House) {
    this.houses[houseIndex] = JSON.parse(JSON.stringify(selectedProfile));
    // Solar
    this.houses[0].pvGen = this.data.solar[3].pvGen;
    this.houses[1].pvGen = this.data.solar[this.solarIndex].pvGen;

    // Battery
    this.houses[1].battery_capacity = this.useBattery ? 10000 : 0;

    this.houses.forEach((house: House) => {
      house.battery_charge = 0;
    });
    for (let i = 0; i < this.timestamps.length; i++) {
      this.data.coordinateExisting(i, this.houses, this.noCommunity);
    }
    this.houses = this.houses.slice(0, 2);
    console.log("Changed profile - new data:", this.houses);
    this.data.coordinateCommunity(this.houses, this.timestamps);
    console.log("Changed profile - new community data:", this.data.community);

    const ref = this.db.object("house" + houseIndex);
    ref.set(this.houses[houseIndex].name).then((resp) => {
      // console.log("Response: ", resp);
    }).catch((error) => {
      console.log("Error: ", error);
    });
  }

  updateHouse(houseIndex: number, profileName: string) {
    let profileIndex: number = 0;
    this.data.houses.some((house: House, index: number) => {
      if (house.name === profileName) {
        profileIndex = index;
        return true;
      }
      return false;
    });
    this.selectProfile(houseIndex, this.data.houses[profileIndex]);
    this.updateTimelineChart();
  }

  selectWeather(houseIndex: number, profileIndex: number) {
    this.solarIndex = profileIndex;
    // Solar
    this.houses[0].pvGen = this.data.solar[3].pvGen; // TODO
    this.houses[1].pvGen = this.data.solar[profileIndex].pvGen;

    this.houses.forEach((house: House) => {
      house.battery_charge = 0;
    });
    for (let i = 0; i < this.timestamps.length; i++) {
      this.data.coordinateExisting(i, this.houses, this.noCommunity);
    }
    this.houses = this.houses.slice(0, 2);
    console.log("Changed weather - new data:", this.houses);
    this.data.coordinateCommunity(this.houses, this.timestamps);
    console.log("Changed profile - new community data:", this.data.community);

    // TODO Currently weather can only be set from tutorial-dashboard
    // const ref = this.db.object("house" + houseIndex);
    // ref.set(this.houses[houseIndex].name).then((resp) => {
    //   // console.log("Response: ", resp);
    // }).catch((error) => {
    //   console.log("Error: ", error);
    // });
  }

  updateWeather(houseIndex: number, profileName: string) {
    let profileIndex: number = 0;
    this.data.solar.some((solar: House, index: number) => {
      if (solar.name === profileName) {
        profileIndex = index;
        return true;
      }
      return false;
    });
    this.selectWeather(houseIndex, profileIndex);
    this.updateTimelineChart();
    this.updateDayNightTransition();
  }

  updateBattery(houseIndex: number, useBattery: boolean) {
    this.useBattery = useBattery;
    this.houses[houseIndex].battery_capacity = useBattery ? 10000 : 0;
    this.houses.forEach((house: House) => {
      house.battery_charge = 0;
    });
    for (let i = 0; i < this.timestamps.length; i++) {
      this.data.coordinateExisting(i, this.houses, this.noCommunity);
    }
    this.houses = this.houses.slice(0, 2);
    console.log("Changed battery - new data:", this.houses);
    this.data.coordinateCommunity(this.houses, this.timestamps);
    console.log("Changed battery - new community data:", this.data.community);
    this.updateTimelineChart();
  }

  /**
   * Set weather type
   * @param houseIndex
   * @param profileName
   */
  setWeather(houseIndex: number, profileName: string) {
    const ref = this.db.object("house" + houseIndex + "weather");
    ref.set(profileName).then((resp) => {
      // console.log("Response: ", resp);
    }).catch((error) => {
      console.log("Error: ", error);
    });
  }

  /**
   * Set battery
   * @param houseIndex
   * @param useBattery
   */
  setBattery(houseIndex: number, useBattery: boolean) {
    const ref = this.db.object("house" + houseIndex + "battery");
    ref.set(useBattery).then((resp) => {
      // console.log("Response: ", resp);
    }).catch((error) => {
      console.log("Error: ", error);
    });
    this.updateBattery(houseIndex, useBattery);
  }

  getStringDuration(duration: number) {
    return (duration - 0.1) + "s";
  }

  /**
   * Select load profile type of the selected house
   * @param houseIndex
   * @param profileName
   */
  setProfile(houseIndex: number, profileName: string) {
    const ref = this.db.object("house" + houseIndex);
    ref.set(profileName).then((resp) => {
      // console.log("Response: ", resp);
    }).catch((error) => {
      console.log("Error: ", error);
    });
  }

  /**
   * Get duration of energy line animation for provided power
   * @param power
   */
  getDelayFromPower(power: number) {
    // let delay = (-2.5 / 1900) * power + 3.1;
    let delay = (-9 / 5000) * power + 7.7;
    if (power === 0) {
      delay = 0;
    } else if (delay > 5) {
      delay = 5;
    } else if (delay < 0.5) {
      delay = 0.5;
    }
    // console.log("Power:", power, " - delay: ", delay);
    return delay;
  }

  // Chart house 2 - left house
  public barChartOptionsHouse2: ChartConfiguration['options'] = {
    responsive: true,
    maintainAspectRatio: false,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      x: {
        grid: {
          display: false
        },
        stacked: true,
        ticks: {
          autoSkip: true,
          maxTicksLimit: 12,
          maxRotation: 0,
          minRotation: 0,
          color: "#d1d1d1"
        },
      },
      y: {
        grid: {
          display: false
        },
        stacked: true,
        ticks: {
          color: "#d1d1d1"
        },
        min: 0,
        max: 6
      },

    },
    plugins: {
      legend: {
        display: false
      }
    },
    elements: {
      point: {
        radius: 0
      }
    }
  };
  public barChartTypeHouse2: ChartType = 'bar';

  public barChartDataHouse2: ChartData<'bar'> = {
    labels: [ "Load" ],
    datasets: [
      { data: [ 0 ],
        backgroundColor: [this.colorGrid] },
      { data: [ 0 ],
        backgroundColor: [this.colorGreen]}
    ]
  };

  // Chart house 1 - right house
  public barChartOptionsHouse1: ChartConfiguration['options'] = this.barChartOptionsHouse2;
  public barChartTypeHouse1: ChartType = 'bar';

  public barChartDataHouse1: ChartData<'bar'> = {
    labels: [ "Load", "Out" ],
    datasets: [
      { data: [ 0, 0 ],
        backgroundColor: [this.colorGrid, "#cb00ae"]},
      { data: [ 0, 0 ],
        backgroundColor: [this.colorGreen, "#cb00ae"]},
      { data: [ 0, 0 ],
        backgroundColor: [this.colorGreen, "#cb00ae"]},
      { data: [ 0, 0 ],
        backgroundColor: [this.colorGrid, this.colorGreen]}
    ]
  };

  // Self
  public chartOptionsSelfConsumption: ChartConfiguration['options'] = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: true,
        labels: {
          font: {
            size: 16
          }
        }
      }
    }
  };
  public chartTypeSelfConsumption: ChartType = 'doughnut';

  public chartDataSelfConsumption: ChartData<'doughnut'> = {
    labels: [],
    datasets: [
      { data: [0, 1],
        rotation: 180,
        borderWidth: 1,
        backgroundColor: ["#69acff", "#a6a6a6"]},
      { data: [0, 1],
        rotation: 180,
        borderWidth: 1,
        backgroundColor: ["#177fff", "#a6a6a6"]}
    ]

  };

  //--
  // Self
  public chartOptionsSelfSufficiency: ChartConfiguration['options'] = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        display: true,
        labels: {
          font: {
            size: 16
          }
        }
      }
    }
  };
  public chartTypeSelfSufficiency: ChartType = 'doughnut';

  public chartDataSelfSufficiency: ChartData<'doughnut'> = {
    datasets: [
      { data: [0, 1],
        rotation: 180,
        borderWidth: 1,
        backgroundColor: ["#9966ff", "#a6a6a6"]},
      { data: [0, 1],
        rotation: 180,
        borderWidth: 1,
        backgroundColor: ["#6417ff", "#a6a6a6"]}
    ]
  };

  // Timeline
  public timelineChartOptions: ChartConfiguration['options'] = {
    responsive: true,
    maintainAspectRatio: false,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      x: {
        grid: {
          display: false
        },
        stacked: true,
        ticks: {
          autoSkip: true,
          maxTicksLimit: 12.4,
          maxRotation: 0,
          minRotation: 0,
          color: "#d1d1d1",
          font: {
            size: 14
          }
        },
      },
      y: {
        grid: {
          display: false
        },
        stacked: true,
        ticks: {
          color: "#d1d1d1",
          font: {
            size: 14
          }
        },
        title: {
          display: true,
          text: "[kW]",
          font: {
            size: 14
          }
        }
      },

    },
    plugins: {
      legend: {
        display: true,
        labels: {
          font: {
            size: 16
          }
        }
      }
    },
    elements: {
      point: {
        radius: 0
      }
    }
  };
  public timelineChartType: ChartType = 'bar';

  public timelineChartData: ChartData<'bar' | 'line'> = {
    labels: [],
    datasets: [
      { data: [] }
    ]
  };

  updateTimelineChart() {
    const housesChartData: House[] = JSON.parse(JSON.stringify(this.houses));
    housesChartData.forEach((house: House) => {
      house.loadProfile.forEach((load: number, i: number) => {
        house.loadProfile[i] = load / 1000;
        house.pvGen[i] = house.pvGen[i] / 1000;
      });
    });

    // Update chart
    this.timelineChartData.labels = this.timestamps;
    this.timelineChartData.datasets = [];
    this.timelineChartData.datasets.push(
      {
        data: housesChartData[0]?.loadProfile,
        label: "Leva hiša - " + this.houses[0]?.name,
        //cubicInterpolationMode: 'monotone',
        //tension: 0.9
        // type: "line",
        borderRadius: 2,
        backgroundColor: this.house2color
      }
    );
    this.timelineChartData.datasets.push(
      {
        data: housesChartData[1]?.loadProfile,
        label: "Desna hiša - " + this.houses[1]?.name,
        //cubicInterpolationMode: 'monotone',
        //tension: 0.9
        // type: "line",
        borderRadius: 2,
        backgroundColor: this.house1color
      }
    );
    // Add PV data
    this.timelineChartData.datasets.push(
      {
        data: housesChartData[1]?.pvGen,
        label: "Proizvodnja energije iz sončne elektrarne",
        type: "line",
        cubicInterpolationMode: 'monotone',
        tension: 0.9,
        borderColor: this.colorGreen,
        backgroundColor: "#000",
        borderWidth: 5
      }
    );
    Chart.defaults.color = "#d1d1d1";
    // this.chart?.update();
    this.timelineChartData = JSON.parse(JSON.stringify(this.timelineChartData));
  }

  makeItRain() {
    //clear out everything
    $('.rain').empty();
    // this.frontRow.nativeElement.empty();

    let increment = -50;
    let drops = "";
    let backDrops = "";

    while (increment < 150) {
      //couple random numbers to use for various randomizations
      //random number between 98 and 1
      let randoHundo = (Math.floor(Math.random() * (98 - 1 + 1) + 1));
      //random number between 5 and 2
      let randoFiver = (Math.floor(Math.random() * (5 - 2 + 1) + 2));
      //increment
      increment += randoFiver;
      //add in a new raindrop with various randomizations to certain CSS properties
      drops += '<div class="drop" style="left: ' + increment + '%; bottom: ' + (randoFiver + randoFiver - 1 + 110) + '%; animation-delay: 0.' + randoHundo + 's !important; animation-duration: 0.5' + randoHundo + 's !important;position: absolute;\n' +
        '  width: 15px;\n' +
        '  height: 120px;\n' +
        '  pointer-events: none;\n' +
        '  animation: drop 0.5s linear infinite;"><div class="stem" style="animation-delay: 0.' + randoHundo + 's !important; animation-duration: 0.5' + randoHundo + 's !important;width: 2px;\n' +
        '  height: 60%;\n' +
        '  margin-left: 7px;\n' +
        '  background: linear-gradient(to bottom, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0.25));\n' +
        '  animation: stem 0.5s linear infinite;"></div><div class="splat" style="animation-delay: 0.' + randoHundo + 's !important; animation-duration: 0.5' + randoHundo + 's !important;width: 15px;\n' +
        '  height: 10px;\n' +
        '  border-top: 2px dotted rgba(255, 255, 255, 0.5);\n' +
        '  border-radius: 50%;\n' +
        '  opacity: 0;\n' +
        '  animation: splat 0.5s linear infinite;"></div></div>';
      // backDrops += '<div class="drop" style="right: ' + increment + '%; bottom: ' + (randoFiver + randoFiver - 1 + 100) + '%; animation-delay: 0.' + randoHundo + 's; animation-duration: 0.5' + randoHundo + 's;"><div class="stem" style="animation-delay: 0.' + randoHundo + 's; animation-duration: 0.5' + randoHundo + 's;"></div><div class="splat" style="animation-delay: 0.' + randoHundo + 's; animation-duration: 0.5' + randoHundo + 's;"></div></div>';
      randoHundo = (Math.floor(Math.random() * (98 - 1 + 1) + 1));
      backDrops += '<div class="drop" style="left: ' + increment + '%; bottom: ' + (randoFiver + randoFiver - 1 + 100) + '%; animation-delay: 0.' + randoHundo + 's !important; animation-duration: 0.5' + randoHundo + 's !important;position: absolute;\n' +
        '  width: 15px;\n' +
        '  height: 120px;\n' +
        '  pointer-events: none;\n' +
        '  animation: drop 0.5s linear infinite;"><div class="stem" style="animation-delay: 0.' + randoHundo + 's !important; animation-duration: 0.5' + randoHundo + 's !important;width: 1px;\n' +
        '  height: 60%;\n' +
        '  margin-left: 7px;\n' +
        '  background: linear-gradient(to bottom, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0.25));\n' +
        '  animation: stem 0.5s linear infinite;"></div><div class="splat" style="animation-delay: 0.' + randoHundo + 's !important; animation-duration: 0.5' + randoHundo + 's !important;width: 15px;\n' +
        '  height: 10px;\n' +
        '  border-top: 2px dotted rgba(255, 255, 255, 0.5);\n' +
        '  border-radius: 50%;\n' +
        '  opacity: 0;\n' +
        '  animation: splat 0.5s linear infinite;"></div></div>';
    }

    $('.rain.front-row').append(drops);
    $('.rain.back-row').append(backDrops);
    // this.frontRow.nativeElement.append(drops);

  }

  // Scenario narrations
  narrationScenario1 = [
    {
      content: "Proizvodnja električne energije ima pomemben vpliv na globalno segrevanje.",
      startTime: 2,
      endTime: 3,
      top: "10%",
      left: "30%",
      width: "40%",
      height: "6%",
      tick: "no-tick"
    },
    {
      content: "Odjem elektrike iz omrežja je označen z oranžno barvo.",
      startTime: 3.5,
      endTime: 4.5,
      top: "82%",
      left: "54%",
      width: "20%",
      height: "6%",
      tick: "left-tick"
    },
    {
      content: "Posameznik trenutno najlažje pridobi zeleno energijo s postavitvijo sončne elektrarne.",
      startTime: 5,
      endTime: 6,
      top: "7%",
      left: "50%",
      width: "30%",
      height: "6%",
      tick: "no-tick"
    },
    {
      content: "Ob sončnem vzhodu sončni paneli začnejo generirati elektriko.",
      startTime: 7.25,
      endTime: 8.25,
      top: "7%",
      left: "50%",
      width: "22%",
      height: "6%",
      tick: "bottom-right-tick"
    },
    {
      content: "Kmalu je proizvedene elektrike dovolj za celotno porabo naprav v hiši.",
      startTime: 8.75,
      endTime: 9.75,
      top: "65%",
      left: "71%",
      width: "22%",
      height: "8%",
      tick: "left-tick"
    },
    {
      content: "Presežek proizvedene energije je oddan v omrežje.",
      startTime: 10.25,
      endTime: 11.25,
      top: "38%",
      left: "32%",
      width: "20%",
      height: "6%",
      tick: "bottom-right-tick"
    },
    {
      content: "Pretok zelene energije je označen z zeleno barvo.",
      startTime: 14.75,
      endTime: 15.75,
      top: "38%",
      left: "32%",
      width: "20%",
      height: "6%",
      tick: "bottom-right-tick"
    },
    {
      content: "Zvečer se zmanjša količina proizvedene elektrike.",
      startTime: 17,
      endTime: 18,
      top: "7%",
      left: "50%",
      width: "22%",
      height: "6%",
      tick: "bottom-right-tick"
    },
    {
      content: "Vsa elektrika se pridobiva iz omrežja.",
      startTime: 20,
      endTime: 21,
      top: "82%",
      left: "54%",
      width: "20%",
      height: "6%",
      tick: "left-tick"
    },
    {
      content: "Vidimo lahko, da profil porabe gospodinjstva običajno ni usklajen s profilom proizvodnje elektrike iz sončne elektrarne.",
      startTime: 21.5,
      endTime: 23.5,
      top: "10%",
      left: "20%",
      width: "60%",
      height: "6%",
      tick: "no-tick"
    }
  ];

  narrationScenario2 = [
    {
      content: "Z množičnimi priklopi sončnih elektrarn bi lahko v urah največje proizvodnje prišlo do preobremenitev obstoječega omrežja.",
      startTime: 2,
      endTime: 3,
      top: "10%",
      left: "20%",
      width: "60%",
      height: "6%",
      tick: "no-tick"
    },
    {
      content: "Rešitev te težave so lahko energetske skupnosti.",
      startTime: 3.5,
      endTime: 4.5,
      top: "10%",
      left: "20%",
      width: "60%",
      height: "6%",
      tick: "no-tick"
    },
    {
      content: "Ena hiša ima na strehi nameščeno sončno elektrarno.",
      startTime: 5,
      endTime: 6,
      top: "7%",
      left: "50%",
      width: "22%",
      height: "6%",
      tick: "bottom-right-tick"
    },
    {
      content: "Ob sončnem vzhodu sončni paneli začnejo generirati elektriko.",
      startTime: 7.25,
      endTime: 8.25,
      top: "7%",
      left: "50%",
      width: "22%",
      height: "6%",
      tick: "bottom-right-tick"
    },
    {
      content: "Kmalu je proizvedene elektrike dovolj za celotno porabo naprav v hiši lastnika sončne elektrarne.",
      startTime: 8.75,
      endTime: 9.75,
      top: "65%",
      left: "71%",
      width: "22%",
      height: "8%",
      tick: "left-tick"
    },
    {
      content: "Presežek proizvedene zelene energije lahko odkupi sosed s katerim sta v energetski skupnosti.",
      startTime: 11.5,
      endTime: 13.25,
      top: "73%",
      left: "3%",
      width: "27%",
      height: "10%",
      tick: "right-tick"
    },
    {
      content: "Pretok zelene energije je označen z zeleno barvo.",
      startTime: 14.75,
      endTime: 15.75,
      top: "74%",
      left: "9%",
      width: "20%",
      height: "6%",
      tick: "right-tick"
    },
    {
      content: "Z lokalno porabo proizvedene elektrike se zmanjša obremenitev omrežja.",
      startTime: 16.25,
      endTime: 17.25,
      top: "78%",
      left: "2%",
      width: "30%",
      height: "8%",
      tick: "right-tick"
    },
    {
      content: "Zvečer se zmanjša količina proizvedene elektrike.",
      startTime: 18,
      endTime: 19,
      top: "7%",
      left: "50%",
      width: "22%",
      height: "6%",
      tick: "bottom-right-tick"
    },
    {
      content: "Obe hiši elektriko ponovno pridobivata iz omrežja.",
      startTime: 20,
      endTime: 21,
      top: "78%",
      left: "11%",
      width: "20%",
      height: "6%",
      tick: "right-tick"
    },
    {
      content: "Z vključevanjem v energetske skupnosti imajo koristi vsi - lastniki sončnih panelov in odjemalci lokalne zelene energije.",
      startTime: 21.5,
      endTime: 23.5,
      top: "10%",
      left: "20%",
      width: "60%",
      height: "6%",
      tick: "no-tick"
    }
  ];

  narrationScenario3 = [
    {
      content: "Proizvodnja elektrike iz sončne elektrarne ne sovpada popolnoma z lokalnimi potrebami po elektriki.",
      startTime: 2,
      endTime: 3,
      top: "10%",
      left: "20%",
      width: "60%",
      height: "6%",
      tick: "no-tick"
    },
    {
      content: "Rešitev so hranilniki energije, ki energijo proizvedeno v času presežka shranijo za čas večjih potreb.",
      startTime: 3.5,
      endTime: 4.5,
      top: "10%",
      left: "20%",
      width: "60%",
      height: "6%",
      tick: "no-tick"
    },
    {
      content: "Najbolj smiselen tip hranilnikov energije so trenutno baterije.",
      startTime: 5,
      endTime: 6,
      top: "10%",
      left: "20%",
      width: "60%",
      height: "6%",
      tick: "no-tick"
    },
    {
      content: "Uporabimo lahko celo baterijo električnega avtomobila.",
      startTime: 6.5,
      endTime: 7.5,
      top: "10%",
      left: "20%",
      width: "60%",
      height: "6%",
      tick: "no-tick"
    },
    {
      content: "Presežek proizvedene energije je lahko shranjen v baterijo.",
      startTime: 8.75,
      endTime: 10.75,
      top: "38%",
      left: "79%",
      width: "20%",
      height: "6%",
      tick: "bottom-left-tick"
    },
    {
      content: "Ko je baterija polna, lahko presežek odkupi sosed s katerim sta v energetski skupnosti.",
      startTime: 11.5,
      endTime: 13.25,
      top: "73%",
      left: "3%",
      width: "27%",
      height: "10%",
      tick: "right-tick"
    },
    {
      content: "Proti večeru se zmanjša količina proizvedene elektrike.",
      startTime: 15.75,
      endTime: 16.75,
      top: "7%",
      left: "50%",
      width: "22%",
      height: "6%",
      tick: "bottom-right-tick"
    },
    {
      content: "Del potrebe po energiji se lahko pokrije iz baterije.",
      startTime: 17.75,
      endTime: 19.25,
      top: "79%",
      left: "50%",
      width: "20%",
      height: "6%",
      tick: "right-tick"
    },
    {
      content: "Tako tudi v času ko ne pridobivamo energije iz sonca lahko uporabljamo lokalno zeleno energijo.",
      startTime: 21.5,
      endTime: 23.5,
      top: "10%",
      left: "20%",
      width: "60%",
      height: "6%",
      tick: "no-tick"
    }
  ];
}

export class Line {
  elementId: string;
  delay: number;
  direction: boolean;
  timeout: any;

  constructor(
    elementId: string,
    delay: number = 0,
    direction: boolean = true,
  ) {
    this.elementId = elementId;
    this.delay = delay;
    this.direction = direction;
  }
}
