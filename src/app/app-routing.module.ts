import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MainComponent} from "./main/main.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {TutorialComponent} from "./tutorial/tutorial.component";
import {TutorialDashboardComponent} from "./tutorial-dashboard/tutorial-dashboard.component";
import {DashboardHome} from "./dashboard-home/dashboard-home";
import {AmbientTime} from "./ambient-time/ambient-time";
import {Questionnaire} from "./questionnaire/questionnaire";

const routes: Routes = [
  { path: 'prkz', component: MainComponent },
  { path: '', component: Questionnaire },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'tutorial', component: TutorialComponent },
  { path: 'dashboard-home', component: DashboardHome },
  { path: 'tutorial-dashboard', component: TutorialDashboardComponent },
  { path: 'ambient-time', component: AmbientTime },
  { path: 'questionnaire', component: Questionnaire }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
