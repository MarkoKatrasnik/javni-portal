import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgChartsModule } from 'ng2-charts';
import {MainComponent} from "./main/main.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {TutorialComponent} from "./tutorial/tutorial.component";
import { initializeApp,provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
import { provideDatabase,getDatabase } from '@angular/fire/database';
import {FIREBASE_OPTIONS} from "@angular/fire/compat";
import {TutorialDashboardComponent} from "./tutorial-dashboard/tutorial-dashboard.component";

import {DashboardHome} from "./dashboard-home/dashboard-home";
import {AmbientTime} from "./ambient-time/ambient-time";
import {Questionnaire} from "./questionnaire/questionnaire";
import {QrCodeModule} from "ng-qrcode";
@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgChartsModule,
    AppRoutingModule,
    QrCodeModule,
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideDatabase(() => getDatabase())
  ],
  declarations: [
    AppComponent,
    MainComponent,
    DashboardComponent,
    TutorialComponent,
    TutorialDashboardComponent,
    DashboardHome,
    AmbientTime,
    Questionnaire],
  bootstrap: [AppComponent],
  providers: [
    { provide: FIREBASE_OPTIONS, useValue: environment.firebase }
  ]
})
export class AppModule {}
