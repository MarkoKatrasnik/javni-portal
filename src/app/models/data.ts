import {House} from "../House";
import {HttpClient} from "@angular/common/http";
import {Appliance} from "../Appliance";
import {Community} from "./community";
import {Prompt} from "../Prompt";

export class Data {
  public houses: House[] = [];
  public community: Community = new Community();

  public solar: House[] = [];
  public prompts: Prompt[] = [];

  time: string = 'Waiting...';
  timestamps: string[] = [];

  currentWeather: string = "SUNNY";

  currentIndex: number = 0;

  constructor(
    private http: HttpClient
  ) {
    this.http
      .get('assets/Zvezek1.csv', { responseType: 'text' })
      .subscribe(
        (data) => {
          let csvToRowArray = data.split('\n');
          for (let index = 1; index < csvToRowArray.length - 1; index++) {
            let row = csvToRowArray[index].split(',');
            this.timestamps.push(row[0].slice(0, -3)); // Timestamps without seconds
          }
          console.log("[dashboard] timestamps:", this.timestamps);
        });
  }

  getHouseData(filename: string) {
    this.http
      .get(filename, { responseType: 'json' })
      .subscribe(
        (data) => {
          console.log("timestamps:", this.timestamps);
          console.log("JSON:", data);
          // @ts-ignore
          data.forEach((houseJson: any) => {
            let newHouseData: House = new House(
              houseJson.appliances as Appliance[]
            );
            newHouseData.name = houseJson.id;
            newHouseData.color = houseJson.color;
            if (houseJson.pvGen !== undefined) {
              newHouseData.pvGen = houseJson.pvGen;
            }
            console.log("houseJson:", newHouseData);
            this.houses.push(newHouseData);
          });

          // Calculate appliances profiles
          for (const house of this.houses) {
            for (const appliance of house.appliances) {
              let profile: number[] = [];
              for (let i = 0; i < this.timestamps.length; i++) {
                let power: number = 0;
                for (const use of appliance.use) {
                  if (use.start * 4 <= i && use.end * 4 > i) {
                    power += appliance.power;
                  }
                }
                profile.push(power);
              }
              appliance.useProfile = profile;
            }
          }
          console.log("Calculated profiles:", this.houses);

          // Calculate house load profile (sum of all appliances)
          for (const house of this.houses) {
            let houseLoadProfile: number[] = [];
            for (let i = 0; i < this.timestamps.length; i++) {
              let load: number = 0;
              for (const appliance of house.appliances) {
                load += appliance.useProfile[i];
              }
              houseLoadProfile.push(load);
            }
            house.loadProfile = houseLoadProfile;
          }

          // Calculate distribution - coordinate
          for (let i = 0; i < this.timestamps.length; i++) {
            this.coordinate(i);
          }
          console.log("Calculated loads:", this.houses);

          // Calculate community profiles
          for (let i = 0; i < this.timestamps.length; i++) {
            let pvGen: number = 0;
            let consumption: number = 0;
            let greenConsumption: number = 0;
            for (const house of this.houses) {
              pvGen += house.pvGen[i];
              consumption += house.loadProfile[i];
              greenConsumption += house.load_pv[i];
              greenConsumption += house.load_community[i];
            }
            this.community.pvGen.push(pvGen);
            let selfConsumption = pvGen > 0 ? greenConsumption / pvGen : 0;
            this.community.selfConsumption.push(
              selfConsumption
            );
            this.community.daySelfConsumption += selfConsumption;

            let selfSufficiency = consumption > 0 ? greenConsumption / consumption : 0;
            this.community.selfSufficiency.push(
              selfSufficiency
            );
            this.community.daySelfSufficiency += selfSufficiency;
          }
          this.community.daySelfConsumption /= this.timestamps.length;
          this.community.daySelfSufficiency /= this.timestamps.length;
          console.log("Community:", this.community);
        },
        (error) => {
          console.log(error);
        }
      );
  }

  getSolarData(filename: string) {
    // Solar data
    this.http
      .get(filename, { responseType: 'json' })
      .subscribe(
        (data) => {
          console.log("Solar JSON:", data);
          // @ts-ignore
          data.forEach((houseJson: any) => {
            let newHouseData: House = new House(
              houseJson.appliances as Appliance[]
            );
            newHouseData.name = houseJson.id;
            newHouseData.color = houseJson.color;
            if (houseJson.pvGen !== undefined) {
              newHouseData.pvGen = houseJson.pvGen;
            }
            this.solar.push(newHouseData);
          });
          console.log("solar from JSON:", this.solar);
        },
        (error) => {
          console.log(error);
        }
      );
  }

  getPromptsData(filename: string, callback: () => void) {
    // Prompts data
    this.http
      .get(filename, { responseType: 'json' })
      .subscribe(
        (data) => {
          console.log("Prompts JSON:", data);
          // @ts-ignore
          data.forEach((promptJson: any) => {
            let newData: Prompt = new Prompt();
            newData.id = promptJson.id;
            newData.message = promptJson.message;
            newData.emoji = promptJson.emoji;
            newData.over = promptJson.over;
            newData.equal = promptJson.equal;
            newData.under = promptJson.under;
            newData.title = promptJson.title;
            newData.text = promptJson.text;

            this.prompts.push(newData);
          });
          console.log("Prompts from JSON:", this.prompts);
          callback();
        },
        (error) => {
          console.log(error);
        }
      );
  }

  coordinate(index: number) {

    let communityOverGen: number = 0;
    this.houses.forEach((house: House) => {
      // Set agent own values
      if (house.pvGen.length > 0) {
        house.load_pv[index] =
          house.pvGen[index] >= house.loadProfile[index] ?
            house.loadProfile[index] :
            house.pvGen[index];
      }
      // Calculate how much community generated energy is available to other agents
      const agentOverGen = house.pvGen[index] - house.loadProfile[index];
      house.to_community[index] = agentOverGen > 0 ? agentOverGen : 0;
      communityOverGen += house.to_community[index];
    });

    // Distribute available community energy
    this.houses.forEach((house: House) => {
      let agentEnergyNeed = house.loadProfile[index] - house.pvGen[index];
      if (agentEnergyNeed > 0) {
        if (communityOverGen > 0) {
          if (communityOverGen > agentEnergyNeed) {
            house.load_community[index] = agentEnergyNeed;
            communityOverGen -= agentEnergyNeed;
            agentEnergyNeed = 0;
          } else {
            house.load_community[index] = communityOverGen;
            agentEnergyNeed -= communityOverGen;
            communityOverGen = 0;
          }
        }
      }
      house.load_grid[index] = agentEnergyNeed > 0 ? agentEnergyNeed : 0;
    });
  }

  coordinateExisting(index: number, existingHouses: House[], noCommunity: boolean) {
    let communityOverGen: number = 0;
    existingHouses.forEach((house: House) => {
      // Set agent own values
      if (house.pvGen.length > 0) {
        house.load_pv[index] =
          house.pvGen[index] >= house.loadProfile[index] ?
            house.loadProfile[index] :
            house.pvGen[index];
      }
    });

    // Battery
    existingHouses.forEach((house: House) => {
      const agentOverGen = house.pvGen[index] - house.loadProfile[index];
      if (agentOverGen > 0) {
        let overCapacity: number = agentOverGen / 4; // Divided by 4, because data is in 15min intervals
        if (house.battery_charge + overCapacity < house.battery_capacity) {
          console.log("Enough capacity - battery charge:", house.battery_charge, " - House:", house.name);
          house.battery_charge += overCapacity > 0 ? overCapacity : 0;
          house.to_battery[index] = agentOverGen;
        } else {
          let leftCapacity = house.battery_capacity - house.battery_charge;
          console.log("Left capacity:", leftCapacity, " - House:", house.name);
          house.battery_charge = house.battery_capacity;
          house.to_battery[index] = leftCapacity > 0 ? (leftCapacity * 4) : 0;
        }
      } else {
        house.to_battery[index] = 0;
      }
    });

    // Get over generation
    existingHouses.forEach((house: House) => {
      house.over_gen[index] = house.pvGen[index] - house.loadProfile[index] - house.to_battery[index];
      house.over_gen[index] = house.over_gen[index] > 0 ? house.over_gen[index] : 0;
    });

    // To community
    if (!noCommunity) {
      existingHouses.forEach((house: House) => {
        house.to_community[index] = house.over_gen[index];
        communityOverGen += house.to_community[index];
      });
    }

    // From battery
    existingHouses.forEach((house: House) => {
      let agentEnergyNeed = house.loadProfile[index] - house.pvGen[index];
      if (agentEnergyNeed > 0) {
        if (house.battery_charge > 0) {
          let batteryPower: number = house.battery_charge * 4;
          if (batteryPower > agentEnergyNeed) {
            house.load_battery[index] = agentEnergyNeed;
            house.battery_charge -= agentEnergyNeed / 4;
          } else {
            house.load_battery[index] = batteryPower;
            house.battery_charge -= batteryPower / 4;
          }
        } else {
          house.load_battery[index] = 0;
        }
      }
    });

    // Distribute available community energy
    existingHouses.forEach((house: House) => {
      let agentEnergyNeed =
        house.loadProfile[index] -
        house.pvGen[index] -
        house.load_battery[index];
      if (agentEnergyNeed > 0) {
        if (communityOverGen > 0) {
          if (communityOverGen > agentEnergyNeed) {
            house.load_community[index] = agentEnergyNeed;
            communityOverGen -= agentEnergyNeed;
            agentEnergyNeed = 0;
          } else {
            house.load_community[index] = communityOverGen;
            agentEnergyNeed -= communityOverGen;
            communityOverGen = 0;
          }
        } else {
          house.load_community[index] = 0;
        }
      }
      house.battery_charge_arr[index] = Math.round((house.battery_charge / house.battery_capacity) * 100);
      // TODO Battery to community
      house.load_grid[index] = agentEnergyNeed > 0 ? agentEnergyNeed : 0;
    });
  }

  coordinateCommunity(existingHouses: House[], timestamps: string[]) {
    this.community.pvGen = [];
    this.community.selfConsumption = [];
    this.community.selfSufficiency = [];
    this.community.daySelfConsumption = 0;
    this.community.daySelfSufficiency = 0;
    let selfConsumptionCounter: number = 0;

    let dayConsumption = 0;
    let dayGreenConsumption = 0;
    let dayPvGen = 0;
    for (let i = 0; i < timestamps.length; i++) {
      let pvGen: number = 0;
      let consumption: number = 0;
      let greenConsumption: number = 0;
      for (const house of existingHouses) {
        pvGen += house.pvGen[i];
        consumption += house.loadProfile[i];
        greenConsumption += house.load_pv[i];
        greenConsumption += house.load_community[i];
        greenConsumption += house.load_battery[i];
      }
      dayPvGen += pvGen;
      this.community.pvGen.push(pvGen);
      this.community.consumption.push(consumption);
      let selfConsumption = pvGen > 0 ? greenConsumption / pvGen : 0;
      this.community.selfConsumption.push(
        selfConsumption
      );
      this.community.daySelfConsumption += selfConsumption;
      if (pvGen > 0) {
        selfConsumptionCounter++;
      }
      dayConsumption += consumption;
      dayGreenConsumption += greenConsumption;

      let selfSufficiency = consumption > 0 ? greenConsumption / consumption : 0;
      this.community.selfSufficiency.push(
        selfSufficiency
      );
    }

    this.community.daySelfSufficiency = dayGreenConsumption / dayConsumption;
    this.community.daySelfConsumption = dayGreenConsumption / dayPvGen;
    console.log("Community:", this.community);
  }
}
