export class Community {
  pvGen: number[] = [];
  consumption: number[] = [];
  selfConsumption: number[] = [];
  daySelfConsumption: number = 0.5;
  selfSufficiency: number[] = [];
  daySelfSufficiency: number = 0.5;
}
