export class Narration {
  content: string = "Obe hiši vso elektriko pridobivata iz omrežja.";
  top: string = "78%";
  left: string = "11%";
  width: string = "20%";
  height: string = "6%";
  opacity: number = 1;
  tick: string = "right-tick";

  setNarrationData(narrationData: any) {
    this.opacity = 1;
    this.content = narrationData.content;
    this.top = narrationData.top;
    this.left = narrationData.left;
    this.height = narrationData.height;
    this.width = narrationData.width;
    this.tick = narrationData.tick;
  }
}
