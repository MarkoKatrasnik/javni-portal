import {Appliance} from "./Appliance";

export class House {
  name: string = "No name";
  color: string = "#FF0000";
  appliances: Appliance[];
  loadProfile: number[] = [];
  pvGen: number[] = new Array(96).fill(0);

  // Results from community coordinator
  load_grid: number[] = new Array(96).fill(0);
  load_community: number[] = new Array(96).fill(0);
  load_pv: number[] = new Array(96).fill(0);
  to_community: number[] = new Array(96).fill(0);
  to_grid: number[] = new Array(96).fill(0);

  to_battery: number[] = new Array(96).fill(0);
  load_battery: number[] = new Array(96).fill(0);
  battery_charge: number = 0;
  battery_capacity: number = 10000;
  battery_charge_arr: number[] = new Array(96).fill(0);
  over_gen: number[] = new Array(96).fill(0);

  constructor(appliances: Appliance[]) {
    this.appliances = appliances;
  }
}
