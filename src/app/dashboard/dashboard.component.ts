import {Component, QueryList, ViewChildren} from "@angular/core";
import {Chart, ChartConfiguration, ChartData, ChartType} from "chart.js";
import {Data} from "../models/data";
import {HttpClient} from "@angular/common/http";
import {BaseChartDirective} from "ng2-charts";
import {AngularFireDatabase} from "@angular/fire/compat/database";

@Component({
  selector: 'dashboard.component',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  @ViewChildren(BaseChartDirective) charts: QueryList<BaseChartDirective> | undefined;

  public data: Data;

  constructor(
    private http: HttpClient,
    private db: AngularFireDatabase
  ) {
    this.data = new Data(this.http);

    setTimeout(() => {
      // Update chart
      this.barChartDataTest.labels = this.data.timestamps;
      this.barChartDataTest.datasets = [];
      for (const house of this.data.houses) {
        this.barChartDataTest.datasets.push(
          {
            data: house.loadProfile,
            label: house.name,
            //cubicInterpolationMode: 'monotone',
            //tension: 0.9
            borderRadius: 2
          }
        )
      }
      // Chart.defaults.color = "#d1d1d1";
      // this.chart?.update();
      this.barChartDataTest = JSON.parse(JSON.stringify(this.barChartDataTest));

      // Update chart House2
      this.barChartDataHouse2.labels = this.data.timestamps;
      this.barChartDataHouse2.datasets = [];
      for (const appliance of this.data.houses[1].appliances) {
        this.barChartDataHouse2.datasets.push(
          {
            data: appliance.useProfile,
            label: appliance.type,
            borderRadius: 2
          }
        )
      }
      this.barChartDataHouse2 = JSON.parse(JSON.stringify(this.barChartDataHouse2));

      // Update chart House2 pv
      this.barChartDataHouse2pv.labels = this.data.timestamps;
      this.barChartDataHouse2pv.datasets = [
        {
          data: this.data.houses[1].pvGen,
          label: "PV generation [W]",
          cubicInterpolationMode: 'monotone',
          tension: 0.9,
          borderColor: "#e6c212",
          backgroundColor: "#FFF"
        }
      ];

      this.barChartDataHouse2pv = JSON.parse(JSON.stringify(this.barChartDataHouse2pv));

    }, 1000);

    // Listen for current time
    const ref = this.db.object("time");
    ref.valueChanges().subscribe((data) => {
      console.log("Time:", data);
      this.data.time = data as string;
    })
  }

  // Chart.js
  public barChartOptionsTest: ChartConfiguration['options'] = {
    responsive: true,
    maintainAspectRatio: false,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      x: {
        grid: {
          display: false
        },
        stacked: true,
        ticks: {
          autoSkip: true,
          maxTicksLimit: 12,
          maxRotation: 0,
          minRotation: 0,
          // color: "#d1d1d1"
        },
      },
      y: {
        grid: {
          display: false
        },
        stacked: true,
        // ticks: {
        //   color: "#d1d1d1"
        // }
      },

    },
    plugins: {
      legend: {
        display: true
      }
    },
    elements: {
      point: {
        radius: 0
      }
    }
  };
  public barChartTypeTest: ChartType = 'bar';

  public barChartDataTest: ChartData<'bar'> = {
    labels: [],
    datasets: [
      { data: []}
    ]
  };

  // House2
  public barChartOptionsHouse2: ChartConfiguration['options'] = {
    responsive: true,
    maintainAspectRatio: false,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      x: {
        grid: {
          display: false
        },
        stacked: true,
        ticks: {
          autoSkip: true,

          maxRotation: 0,
          minRotation: 0,
          // color: "#d1d1d1"
        },
      },
      y: {
        grid: {
          display: false
        },
        stacked: true,
        // ticks: {
        //   color: "#d1d1d1"
        // }
      },

    },
    plugins: {
      legend: {
        display: true
      }
    },
    elements: {
      point: {
        radius: 0
      }
    }
  };
  public barChartTypeHouse2: ChartType = 'bar';

  public barChartDataHouse2: ChartData<'bar'> = {
    labels: [],
    datasets: [
      { data: [] }
    ]
  };

  // House2 pv
  public barChartOptionsHouse2pv: ChartConfiguration['options'] = {
    responsive: true,
    maintainAspectRatio: false,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      x: {
        grid: {
          display: false
        },
        stacked: true,
        ticks: {
          autoSkip: true,
          maxTicksLimit: 12,
          maxRotation: 0,
          minRotation: 0,
          // color: "#d1d1d1"
        },
      },
      y: {
        grid: {
          display: false
        },
        stacked: true,
        // ticks: {
        //   color: "#d1d1d1"
        // }
      },

    },
    plugins: {
      legend: {
        display: true
      }
    },
    elements: {
      point: {
        radius: 0
      }
    }
  };
  public barChartTypeHouse2pv: ChartType = 'line';

  public barChartDataHouse2pv: ChartData<'line'> = {
    labels: [],
    datasets: []
  };

}
