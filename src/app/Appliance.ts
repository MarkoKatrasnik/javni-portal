export class Appliance {
  type: string;
  // Appliance power in Watts
  power: number;
  // Array with start and end times of appliance uses
  use: Use[];
  // Time in hours by which start of use can be delayed
  flexibility: number;
  useProfile: number[] = [];

  constructor(data: any) {
    this.type = data.type;
    this.power = data.power;
    this.use = data.use;
    this.flexibility = data.flexibility;
  }
}

export class Use {
  start: number;
  end: number;
  constructor(start: number, end: number) {
    this.start = start;
    this.end = end;
  }
}
